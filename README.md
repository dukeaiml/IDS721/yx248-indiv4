# yx248-indiv4

![pipeline status](https://gitlab.com/dukeaiml/IDS721/yx248-indiv4/badges/main/pipeline.svg)

## Rust AWS Lambda for Sentiment Analysis

This project implements a sentiment analysis pipeline using AWS services orchestrated by AWS Step Functions. The pipeline comprises two main AWS Lambda functions written in Rust:

- `rust-hf-lambda`: This function is triggered by an HTTP GET request, processes the text for sentiment analysis, and outputs the result in `input_stage1.json`.
- `hf_result_test_lambda`: This function takes the output from `rust-hf-lambda`, further analyzes the sentiment score, and determines the validity of the result. A score below 70% is deemed uncertain (`false`), while a score above 70% is considered a conclusive analysis (`true`). The results are output in `output.json`.


## System Architecture

Below is the Step Functions workflow diagram showing the orchestration of the Lambda functions:

![Step Functions Workflow Graph](images/Step_Functions_workflow_Graph.png)

The `Step_Functions_workflow_Graph.png` illustrates the orchestration process within our data processing pipeline. This diagram is key to understanding the flow and execution order of the functions within our pipeline, and will be explained in detail in the demo video.


### Data Processing Pipeline

1. An HTTP GET request is received by `rust-hf-lambda`, initiating the sentiment analysis. The input for this stage is [`input_stage1.json`](input_stage1.json).
2. The analyzed data is passed to `hf_result_test_lambda` for further processing. This stage's input and output from the previous stage are in [`input_stage2.json`](input_stage2.json).
3. Depending on the sentiment score, a boolean validity (`true` or `false`) is output to [`output.json`](output.json).


## Installation and Deployment

### Prerequisites

- An AWS account
- AWS CLI configured
- Rust environment setup with Cargo

### Deploying Lambda Functions

Use the following commands to deploy the Lambda functions:

```bash
# Deploy rust-hf-lambda
$ cargo lambda build --release
$ cargo lambda deploy --your-lambda-function-role-id
```

## Usage

To initiate the sentiment analysis, trigger an HTTP GET request to rust-hf-lambda with the required text:

```url
https://34ct5jlwn1.execute-api.us-east-2.amazonaws.com/mini10-rust-hf-lambda?text=I%20love%20rust
```

The Step Functions workflow will manage the execution of the analysis and processing tasks as shown in the screenshots of the workflow execution:

![Step_Functions_workflow_task1](images/Step_Functions_workflow_task1.png)
![Step_Functions_workflow_task2](images/Step_Functions_workflow_task2.png)
![Step_Functions_workflow_task_result](images/Step_Functions_workflow_task_result.png)


## Demo Video Link

[Demo Vido Link](https://youtu.be/4Ri56rJiyqM)
