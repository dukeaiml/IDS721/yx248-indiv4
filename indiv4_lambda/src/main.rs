use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use regex::Regex;

#[derive(Debug, Deserialize)]
struct HttpResponse {
    statusCode: Option<u16>,
    headers: Option<Headers>,
    multiValueHeaders: Option<MultiValueHeaders>,
    body: String,
    isBase64Encoded: Option<bool>,
    cookies: Option<Vec<String>>,
}

#[derive(Debug, Deserialize)]
struct Headers {
    content_type: Option<String>,
}

#[derive(Debug, Deserialize)]
struct MultiValueHeaders {
    content_type: Option<Vec<String>>,
}

#[derive(Debug, Deserialize)]
struct SentimentResult {
    result: Option<String>,
    message: Option<String>,
    error: Option<String>,
}

#[derive(Debug, Serialize)]
struct LambdaResponse {
    valid: String,
}

async fn function_handler(event: Value, _: Context) -> Result<serde_json::Value, Error> {
    // Deserialize the HTTP response
    let http_response: HttpResponse = serde_json::from_value(event)?;

    // Parse the nested JSON in the body field
    let sentiment_result: SentimentResult = serde_json::from_str(&http_response.body)
        .unwrap_or(SentimentResult {
            result: None,
            message: None,
            error: None,
        });
    // let sentiment_result: SentimentResult = serde_json::from_value(event).unwrap_or(SentimentResult {
    //     result: None,
    //     message: None,
    //     error: None,
    // });

    if let Some(result) = sentiment_result.result {
        let re = Regex::new(r"polarity: (\w+), score: (\d+\.\d+)").unwrap();

        if let Some(caps) = re.captures(&result) {
            if let (Some(polarity), Some(score_str)) = (caps.get(1), caps.get(2)) {
                let score: f64 = score_str.as_str().parse().unwrap_or(0.0);
                if (polarity.as_str() == "Positive" || polarity.as_str() == "Negative") && score > 0.7 {
                    return Ok(json!({"valid": "true"}));
                }
            }
        }

        return Ok(json!({"valid": "false"}));
    }  else if sentiment_result.message.is_some() {
        // Handle message JSON structures
        return Ok(json!({"valid": "message"}));
    } else if sentiment_result.error.is_some() {
        // Handle error JSON structures
        return Ok(json!({"valid": "error"}));
    }

    // Default response if none of the conditions are met
    Ok(json!({"valid": "unknown"}))
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
